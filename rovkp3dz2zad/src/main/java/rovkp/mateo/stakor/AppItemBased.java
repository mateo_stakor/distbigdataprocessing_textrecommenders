package rovkp.mateo.stakor;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.file.FileItemSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.ItemBasedRecommender;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AppItemBased {

    private static final int NUM_RECOMMENDATIONS = 10;
    private static final int NUM_USERS = 100;

    public static void main( String[] args ) throws IOException, TasteException {
        if (args.length != 3) {
            throw new IllegalArgumentException("Please provide 2 input paths and output path");
        }

        DataModel model = new FileDataModel(new File(args[0]), "\t\t");
        ItemSimilarity similarity = new FileItemSimilarity(new File(args[1]));
        ItemBasedRecommender recommender = new GenericItemBasedRecommender(model, similarity);

        getRecommendationsForUser(recommender, 22L, NUM_RECOMMENDATIONS).forEach(r -> {
            System.out.println(r.getItemID());
        });

        //List<Long> userIds = getUsers(model, NUM_USERS);
        //writeRecommendationsToFile(args[2], recommender, userIds);
    }

    private static void writeRecommendationsToFile(String filePath, ItemBasedRecommender recommender, List<Long> userIds) throws IOException {
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
        userIds.forEach(uId -> {
            try {
                getRecommendationsForUser(recommender, uId, NUM_RECOMMENDATIONS)
                        .forEach(r -> {
                            try {
                                writter.append(String.format(Locale.US, "%d,%d", uId, r.getItemID()));
                                writter.newLine();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
            } catch (TasteException e) {
                e.printStackTrace();
            }
        });

        writter.close();
    }

    private static List<RecommendedItem> getRecommendationsForUser(ItemBasedRecommender recommender, Long userId, int numOfRecommendations) throws TasteException {
        return recommender.recommend(userId, numOfRecommendations);
    }

    private static List<Long> getUsers(DataModel model, int numOfUsers) throws TasteException {
        List<Long> userIds = new ArrayList<>();
        LongPrimitiveIterator iterator = model.getUserIDs();
        while(iterator.hasNext()) {
            userIds.add(iterator.next());
            if (userIds.size() >= numOfUsers) break;
        }
        return userIds;
    }
}
