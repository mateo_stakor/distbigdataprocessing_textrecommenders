package rovkp.mateo.stakor;

import java.io.File;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.file.FileItemSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.ItemBasedRecommender;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.apache.mahout.common.RandomUtils;

import java.io.File;
import java.io.IOException;

public class Evaluator {
    public static void main( String[] args ) throws IOException, TasteException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Please provide input path");
        }

        DataModel model = new FileDataModel(new File(args[0]), "\t\t");
        RecommenderBuilder userBased = getUserBasedRecommenderBuilder();
        RecommenderBuilder itemBased = getItemBasedRecommenderBuilder(args[1]);

        RecommenderEvaluator recEvaluator = new RMSRecommenderEvaluator();
        System.out.println(String.format("User based: %f", recEvaluator.evaluate(userBased, null, model, 0.3, 0.3)));
        System.out.println(String.format("Item based: %f", recEvaluator.evaluate(itemBased, null, model, 0.3, 0.3)));
    }

    private static RecommenderBuilder getUserBasedRecommenderBuilder() {
        return m -> {
            UserSimilarity similarity = new PearsonCorrelationSimilarity(m);
            UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, m);
            return new GenericUserBasedRecommender(m, neighborhood, similarity);
        };
    }

    private static RecommenderBuilder getItemBasedRecommenderBuilder(String input) {
        return m -> {
            ItemSimilarity similarity = new FileItemSimilarity(new File(input));
            return new GenericItemBasedRecommender(m, similarity);
        };
    }
}
