package rovkp.mateo.stakor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.common.Weighting;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.LogLikelihoodSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.ItemBasedRecommender;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

public class AppUserBased
{
    private static final int NUM_RECOMMENDATIONS = 10;
    private static final int NUM_USERS = 100;

    public static void main( String[] args ) throws IOException, TasteException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Please provide input path");
        }

        DataModel model = new FileDataModel(new File(args[0]), "\t\t");

        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1d, similarity, model);
        UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);

        getRecommendationsForUser(recommender, 22L, NUM_RECOMMENDATIONS).forEach(r -> {
            System.out.println(r.getItemID());
        });

        //writeRecommendationsToFile(args[1], recommender, getUsers(model, NUM_USERS));
    }

    private static List<RecommendedItem> getRecommendationsForUser(UserBasedRecommender recommender, Long userId, int numOfRecommendations) throws TasteException {
        return recommender.recommend(userId, numOfRecommendations);
    }

    private static void writeRecommendationsToFile(String filePath, UserBasedRecommender recommender, List<Long> userIds) throws IOException {
        BufferedWriter writter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
        userIds.forEach(uId -> {
            try {
                getRecommendationsForUser(recommender, uId, NUM_RECOMMENDATIONS)
                        .forEach(r -> {
                            try {
                                writter.append(String.format(Locale.US, "%d,%d", uId, r.getItemID()));
                                writter.newLine();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
            } catch (TasteException e) {
                e.printStackTrace();
            }
        });

        writter.close();
    }

    private static List<Long> getUsers(DataModel model, int numOfUsers) throws TasteException {
        List<Long> userIds = new ArrayList<>();
        LongPrimitiveIterator iterator = model.getUserIDs();
        while(iterator.hasNext()) {
            userIds.add(iterator.next());
            if (userIds.size() >= numOfUsers) break;
        }
        return userIds;
    }
}
