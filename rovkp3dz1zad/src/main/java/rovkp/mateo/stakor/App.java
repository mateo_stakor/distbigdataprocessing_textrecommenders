package rovkp.mateo.stakor;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final String TEXT_FIELD = "text";
    private static final String ID_FIELD = "ID";

    public static void main( String[] args ) throws IOException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Please provide path (input and output)");
        }

        Map<Integer, String> jokes = extractJokesFromDocument(args[0]);

        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
        Directory index = new RAMDirectory();

        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        IndexWriter writer = new IndexWriter(index, config);
        addDocuments(writer, jokes);

        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        double[][] matrix = createNormalizedSimilarityMatrix(analyzer, searcher, jokes);
        writeNormalizedSimilarityMatrixToFile(matrix, jokes.size(), args[1]);

        reader.close();

        System.out.println(findMostSimilarJoke(matrix, jokes.size(), 100));
    }

    private static Map<Integer, String> extractJokesFromDocument(String docPath) throws IOException {
        Map<Integer, String> jokes = new HashMap<Integer, String>();
        BufferedReader reader = new BufferedReader(new FileReader(docPath));
        String line = reader.readLine();
        while (line != null) {
            Integer jokeId = Integer.parseInt(line.replaceAll(":",""));

            StringBuilder jokeTextHtml = new StringBuilder();
            line = reader.readLine();
            while (!line.isEmpty()) {
                jokeTextHtml.append(line);
                line = reader.readLine();
            }

            String jokeText = StringEscapeUtils.unescapeXml(jokeTextHtml.toString().toLowerCase().replaceAll("\\<.*?\\>", " ")).replaceAll("[^\\d\\w\\s]*", "");

            jokes.put(jokeId, jokeText);
            line = reader.readLine();
        }

        reader.close();

        return jokes;
    }

    private static void writeNormalizedSimilarityMatrixToFile(double[][] matrix, int size, String outPath) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outPath, false));

        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                if (matrix[i][j] > 0) {
                    writer.write(String.format(Locale.US, "%d,%d,%f", i + 1, j + 1, matrix[i][j]));
                    writer.newLine();
                }
            }
        }

        writer.close();
    }

    private static void addDocuments(IndexWriter writer, Map<Integer, String> jokes) throws IOException {

        jokes.forEach((k, v) -> {
            try {
                Document document = new Document();
                document.add(new TextField(TEXT_FIELD, v, Field.Store.YES));
                document.add(new StringField(ID_FIELD, k.toString(), Field.Store.YES));
                writer.addDocument(document);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        writer.close();
    }

    private static double[][] createNormalizedSimilarityMatrix(StandardAnalyzer analyzer, IndexSearcher indexSearcher, Map<Integer, String> jokes) {
        Integer size = jokes.size();
        double[][] matrix = new double[size][size];

        jokes.forEach((k, v) -> {
            try {
                Query q = new QueryParser(Version.LUCENE_40, TEXT_FIELD, analyzer).parse(v);
                TopDocs topDocs = indexSearcher.search(q, size);
                for (ScoreDoc doc : topDocs.scoreDocs) {
                    matrix[k - 1][Integer.parseInt(indexSearcher.doc(doc.doc).get(ID_FIELD)) - 1] = doc.score / topDocs.getMaxScore();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                double avg = (matrix[i][j] + matrix[j][i]) / 2d;
                matrix[i][j] = matrix[j][i] = avg;
            }
        }

        return matrix;
    }

    private static int findMostSimilarJoke(double[][] matrix, int size, int compareToDocId) {
        int maxSimilarityIndex = -1;
        double maxSimilarity = 0d;
        for (int i = 0; i < size; i++) {
            if (i == compareToDocId - 1) continue;

            double currSimilarity = matrix[compareToDocId - 1][i];
            if (currSimilarity > maxSimilarity) {
                maxSimilarity = currSimilarity;
                maxSimilarityIndex = i;
            }
        }

        return maxSimilarityIndex + 1;
    }
}
